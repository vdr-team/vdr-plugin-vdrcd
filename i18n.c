#include "i18n.h"

const tI18nPhrase Phrases[] = {
  { "Play Disc",     				// English
    "Disk abspielen",				// Deutsch
    "",                     // Slovenski
    "",                     // Italiano
    "",                     // Nederlands
    "",                     // Portugus
		"Lecture du disque",    // Franais
    "",                     // Norsk
    "",                     // Suomi
    "",                     // Polski
    "",                     // Espaol
    "",                     // Ellinika
    "",                     // Svenska
    "",                     // Romaneste
    "",                     // Magyar
		"",											// Catal�
  },
  { "Identifying Disc",     // English
    "Identifiziere Disk",		// Deutsch
    "",                     // Slovenski
    "",                     // Italiano
    "",                     // Nederlands
    "",                     // Portugus
		"Identification du disque",// Franais
    "",                     // Norsk
    "",                     // Suomi
    "",                     // Polski
    "",                     // Espaol
    "",                     // Ellinika
    "",                     // Svenska
    "",                     // Romaneste
    "",                     // Magyar
		"",											// Catal�
  },
  { "Couldn't identify disc!",// English
    "Konnte Disk nicht identifizieren!",// Deutsch
    "",                     // Slovenski
    "",                     // Italiano
    "",                     // Nederlands
    "",                     // Portugus
    "Impossible d'identifier le disque !",// Franais
    "",                     // Norsk
    "",                     // Suomi
    "",                     // Polski
    "",                     // Espaol
    "",                     // Ellinika
    "",                     // Svenska
    "",                     // Romaneste
    "",                     // Magyar
		"",											// Catal�
  },
  { "Missing appropriate PlugIn!",// English
    "Kein passendes PlugIn gefunden!",// Deutsch
    "",                     // Slovenski
    "",                     // Italiano
    "",                     // Nederlands
    "",                     // Portugus
    "Plugin appropri� manquant !",// Franais
    "",                     // Norsk
    "",                     // Suomi
    "",                     // Polski
    "",                     // Espaol
    "",                     // Ellinika
    "",                     // Svenska
    "",                     // Romaneste
    "",                     // Magyar
		"",											// Catal�
  },
  { "Drive not present in mp3sources.conf!"//English
    "Laufwerk fehlt in mp3sources.conf!",// Deutsch
    "",                     // Slovenski
    "",                     // Italiano
    "",                     // Nederlands
    "",                     // Portugus
    "Lecteur absent de mp3sources.conf!",// Franais
    "",                     // Norsk
    "",                     // Suomi
    "",                     // Polski
    "",                     // Espaol
    "",                     // Ellinika
    "",                     // Svenska
    "",                     // Romaneste
    "",                     // Magyar
		"",											// Catal�
  },
  { "Drive not present in mplayersources.conf!"//English
    "Laufwerk fehlt in mplayersources.conf!",// Deutsch
    "",                     // Slovenski
    "",                     // Italiano
    "",                     // Nederlands
    "",                     // Portugus
    "Lecteur absent de mplayersources.conf!",// Franais
    "",                     // Norsk
    "",                     // Suomi
    "",                     // Polski
    "",                     // Espaol
    "",                     // Ellinika
    "",                     // Svenska
    "",                     // Romaneste
    "",                     // Magyar
		"",											// Catal�
  },
	{ NULL }
};

