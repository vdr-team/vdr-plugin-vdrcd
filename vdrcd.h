#ifndef VDR_VDRCD_H
#define VDR_VDRCD_H

#include <stdio.h>

#define DEBUG_VDRCD

#if defined DEBUG_VDRCD
#	define DPRINT(x...) fprintf(stderr, x)
#else
# define DPRINT(x...)
#endif

#endif // VDR_VDRCD_H
